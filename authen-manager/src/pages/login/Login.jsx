import React from 'react';
import './login.css'
import { Link } from 'react-router-dom'

const Login = () => {


  return (
    <div className='login section__padding'>
      <div className="login-container">
        <h1>Đăng nhập</h1>
        <form className='login-writeForm' autoComplete='off'>
          <div className="login-formGroup">
            <label>Tên đăng nhập</label>
            <input type="text" placeholder='Username' />
          </div>
          <div className="login-formGroup">
            <label>Mật khẩu</label>
            <input type="password" placeholder='Password' />
          </div>

          <div className="login-button">
            <button className='login-writeButton' type='submit'>Đăng nhập</button>
            <Link to="/register">
              <button className='login-reg-writeButton' type='submit'>Đăng ký</button>
            </Link>
          </div>
        </form>
      </div>
    </div>
  )
};

export default Login;
