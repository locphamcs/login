import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from '@chakra-ui/react'
import { Button, ButtonGroup } from '@chakra-ui/react'
import './home.css'
import ModalAdd from "../../components/modal/ModalAdd"
import { getAllUsers, deleteUser } from "../../redux/actions/user";
import { useToast } from "@chakra-ui/react";
import ModalEdit from "../../components/modaledit/ModalEdit";

const Home = () => {

  const [openModal, setOpenModal] = useState(false);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [data, setData] = useState("");

  const toast = useToast();

  const dispatch = useDispatch();
  const { users } = useSelector((state) => state);

  useEffect(() => {
    dispatch(getAllUsers()).then(
      (res) => {
      }
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const toggle = (props) => {
    setOpenModal(!openModal)
  }

  const toggleEdit = (props) => {
    setOpenModalEdit(!openModalEdit)
  }

  const handleDelete = (id) => {
    deleteUser(id).then((res) => {
      toast({
        title: "Success!",
        description: "Xoá thành công!",
        status: "success",
        duration: 4000,
        isClosable: true,
        position: "top-right",
      });
      dispatch(getAllUsers())
    })
  }

  return (
    <div
      className="container"
    >
      <Button colorScheme='blue' style={{ "width": "10rem", height: "30px" }} size="md" onClick={toggle}
      >Thêm tài khoản</Button>
      <TableContainer >
        <Table variant='striped'>
          <TableCaption placement="top" >Danh sách tài khoản</TableCaption>
          <Thead>
            <Tr>
              <Th>Mã</Th>
              <Th>Tên</Th>
              <Th >Ngày sinh</Th>
              <Th >Quê quán</Th>
              <Th >Chức vụ</Th>
              <Th >Phòng ban</Th>
              <Th >Hành động</Th>
            </Tr>
          </Thead>
          <Tbody>
            <Tr>
              <Td>inches</Td>
              <Td>millimetres (mm)</Td>
              <Td >25.4</Td>
              <Td>inches</Td>
              <Td>millimetres (mm)</Td>
              <Td >25.4</Td>
              <Td >
                <ButtonGroup >
                  <Button size="sm" colorScheme='teal'>Sửa</Button>
                  <Button size="sm" colorScheme='red'>Xoá</Button>
                </ButtonGroup>
              </Td>
            </Tr>
            <Tr>
              <Td>inches</Td>
              <Td>millimetres (mm)</Td>
              <Td >25.4</Td>
              <Td>inches</Td>
              <Td>millimetres (mm)</Td>
              <Td >25.4</Td>
              <Td >
                <ButtonGroup >
                  <Button size="sm" colorScheme='teal'>Sửa</Button>
                  <Button onClick={handleDelete} size="sm" colorScheme='red'>Xoá</Button>
                </ButtonGroup>
              </Td>
            </Tr>
          </Tbody>
        </Table>
      </TableContainer>
      {openModal &&
        <ModalAdd
          openModal={openModal}
          toggleModal={toggle}
        >
        </ModalAdd>
      }
      {openModal &&
        <ModalEdit
          openModal={openModalEdit}
          toggleModal={toggleEdit}
          data={data}
        >
        </ModalEdit>
      }
    </div>
  );
};

export default Home;
