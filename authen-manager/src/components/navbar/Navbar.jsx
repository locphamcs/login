import React, { useState } from "react";
import "./navbar.css";
import { Link } from "react-router-dom";

const Menu = () => (
    <>
        <Link to="/explore">
            <p>Explore</p>{" "}
        </Link>
    </>
);

const Navbar = () => {

    return (
        <div className="navbar1">
            <div className="navbar-links">
                <div className="navbar-links_logo">
                    <Link to="/">
                        <h1>Quản lý nhân sự</h1>
                    </Link>
                </div>
            </div>
            <div className="navbar-sign">
                {/* <button
                    type="button"
                    className="secondary-btn"
                // onClick={account ? openWallet : connect}
                >
                    Connect
                </button> */}
            </div>
        </div>
    );
};

export default Navbar;