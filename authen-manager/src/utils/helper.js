import {toast} from "react-toastify";

export function setStorage(key, value, expires) {

  if (expires === undefined || expires === null) {
    expires = (24 * 60 * 60);  // default: seconds for 1 day
  } else {
    expires = Math.abs(expires); //make sure it's positive
  }

  let now = Date.now();  //millisecs since epoch time, lets deal only with integer

  let schedule = now + expires * 1000;

  console.log("time: ", expires)

  try {
    localStorage.setItem(key, value);
    localStorage.setItem(key + '_expiresIn', schedule);
  } catch (e) {
    console.log('setStorage: Error setting key [' + key + '] in localStorage: ' + JSON.stringify(e));
    return false;
  }
  return true;
}

export function toastCustom(message, type, position = "top-center") {
  switch (type) {
    case "success":
      toast.success(message, {
        position,
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      break;
    case "error":
      toast.error(message, {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      break;
    case "warn":
      toast.warn(message, {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      break;
    default:
      return;
  }
}

export const returnCurrentPage = (total, pageInfo) => {
  // if (isCheckbox) columns.splice(0, 0, checkBoxColumns);
  let currentPage = 0;
  if (Math.ceil(total / pageInfo.pageSize) < pageInfo.pageIndex + 1) {
    currentPage = Math.ceil(pageInfo.total / pageInfo.pageSize);
  } else {
    currentPage = pageInfo.pageIndex;
  }
  return currentPage;
}
export const sumAllByKey = (arr, key1, key2 = "") => {
  function total(item, _key1, _key2) {
    return item[_key1] || item[_key2];
  }

  function sum(prev, next) {
    return prev + next;
  }

  return arr.map(item => total(item, key1, key2)).reduce(sum);
}

export function getUniqueListBy(arr, key) {
  return [...new Map(arr.map(item => [item[key], item])).values()]
}
