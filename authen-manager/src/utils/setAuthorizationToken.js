// import axiosInstance from '../components/Common/axiosInstance.js';

import axiosInstance from "../axiosInstance";

export default function setAuthorizationToken(token) {
    if (token) {
        axiosInstance.defaults.headers.common['authorization'] = `Bearer ${token}`;
    } else {
        axiosInstance.defaults.headers.common['authorization'] = '';
    }
}
