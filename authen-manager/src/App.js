import "./App.css";
import React from "react";
import { Navbar, Footer } from "./components";
import { Home, Login } from "./pages";
import { Switch, Route } from "react-router-dom";
function App() {

  return (
    <div>
      <Navbar />
      <Switch>
        <Route exact path="/" component={Login}></Route>
        <Route exact path="/login" component={Login}></Route>
        <Route exact path="/home" component={Home}></Route>
        <Route exact path="/register" component={Login}></Route>

        {/* <Route exact path="/explore" component={Export}></Route>
        <Route exact path="/profile" component={Profile}></Route>
        <Route exact path="/create" component={Create}></Route>
        <Route exact path="/post/:id" component={Item}></Route>
        <Route exact path="/detail/:id" component={DetailNFT}></Route>
        <Route exact path="/profile/:id" component={Profile}></Route> */}
      </Switch>
    </div>
  );
}

export default App;
