import axiosInstance from "./../../axiosInstance";
import setAuthorizationToken from "../../utils/setAuthorizationToken";
import { setStorage } from "../../utils/helper";

export const getNonce = (data) => {
  return axiosInstance.post("/auth/getNonce", data);
};

export const login = user => {
    return async dispatch => {
      try {
        let {data} = await axiosInstance.post('/auth/login', user)
        // let {data: system} = await axiosInstance.get('/system');
        localStorage.setItem("token", data.token);
        localStorage.setItem("walletAddress", data.data.walletAddress);
        dispatch(setToken(data.token, null));
        // dispatch({type: Type.LOGIN_ADMIN, user: data.userInfo, ip: data.ip, OS: data.OS, permission:data.userInfo.permission });
        return {
          status: true,
          data
        }
      } catch (e) {
        throw (e)
      }
    }
  }
  
  export function setToken(token, user) {
    return dispatch => {
      setStorage('token', token, parseInt(180 * 60 * 6));
      setAuthorizationToken(token);
    }
  }
  
  export function logout() {
    return dispatch => {
      localStorage.removeItem('token');
      localStorage.removeItem('token_expiresIn');
    //   dispatch(setCurrentUser(null));
      delete axiosInstance.defaults.headers.common["Authorization"];
      // dispatch(setCurrentUser(null));
    }
  }
  
//   export function setCurrentUser(user) {
//     return {
//       type: "CLEAR_STORE",
//       payload: {
//         data: user
//       }
//     };
//   }