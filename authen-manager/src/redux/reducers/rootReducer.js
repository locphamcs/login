import * as Types from "../../utils/Constants";
import produce from "immer";

const initialState = {
    isOpen:false,
    nfts:[]
};

const reducer = (state = initialState, action) => {
  return produce(state, (draft) => {
    switch (action.type) {
      case "LOGIN_ADMIN": {
        return {...state, user: action.user}
      }
      case "CHANGE_ROLE": {
        return {...state, userRole: action.userRole};
      }
      case Types.GET_NFTS:
        draft.nfts = action.nfts.data;
        draft.total = action.nfts.total;
        break;
      case Types.IS_OPEN:
        return {
          ...state,
          isOpen: action.open,
        };
      default:
        return state;
    }
  });
};

export default reducer;
